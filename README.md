# Image Based Geolocation Client #

Android client application of the image based geolocation service running on our server. This is the thin client version of the application, where we are removing the dependency to openCV's Android SDK.

## How to use ##

Press the 'capture' button when you have the desired scene represented in the camera view. The picture taken will be sent to the server and geolocated, a message describing your current location will then be received and presented.