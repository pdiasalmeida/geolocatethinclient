package imageLocator.main;

import android.app.Application;

public class GeoLAPP extends Application
{
	public static final String PREFERENCE_KEY_PREF_SERVER = "server_address_preference";
	public static final String PREFERENCE_KEY_PREF_LOCATION = "location_preference";
}
