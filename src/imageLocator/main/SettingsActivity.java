package imageLocator.main;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class SettingsActivity extends PreferenceActivity
{
	
    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate(savedInstanceState);
        
        PreferenceManager.setDefaultValues( getBaseContext(), R.xml.preferences, false );
        addPreferencesFromResource(R.xml.preferences);
    }

}
