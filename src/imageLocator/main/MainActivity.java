package imageLocator.main;

import imageLocator.location.MyLocationListener;
import imageLocator.network.NetworkHelper;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends Activity implements OnSharedPreferenceChangeListener
{
	private Camera mCamera;
    private CameraPreview mPreview;
    public SharedPreferences sharedPref;
	
	private boolean useLocation;
	private MyLocationListener mll;
	
	private NetworkHelper nth;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_main);
		
		sharedPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
		String upLoadServerImageUri = sharedPref.getString( GeoLAPP.PREFERENCE_KEY_PREF_SERVER,
				"http://192.168.1.109:8000/uploadImage" );
		useLocation = sharedPref.getBoolean( GeoLAPP.PREFERENCE_KEY_PREF_LOCATION, true );
        
		nth = new NetworkHelper( this, upLoadServerImageUri );
		
		if( useLocation )
		{
			LocationManager lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);
			mll = new MyLocationListener( lm, this );
		}
		
		setupCamera();
		
        // Add a listener to the Capture button
    	final Button captureButton = (Button) findViewById(R.id.button_capture);
    	captureButton.setOnClickListener( new View.OnClickListener()
    	{ 
    		public void onClick( View v )
    		{
    			if( mll != null && mll.hasLocation() )
    			{
    				Camera.Parameters params = mCamera.getParameters();
    				params.setGpsAltitude(mll.currentLocation.getAltitude());
    				params.setGpsLatitude(mll.currentLocation.getLatitude());
    				params.setGpsLongitude(mll.currentLocation.getLongitude());
    				
    				mCamera.setParameters(params);
    			}
    			
    			mCamera.takePicture( null, null, mPicture );
    		}
    	});
	}
	
	@Override
    protected void onPause()
	{
        super.onPause();
        releaseCamera();
    }
	
	@Override
    protected void onRestart()
	{
        super.onRestart();
        
        setupCamera();
    }
	
	@Override
    protected void onResume()
    {
        super.onResume();
        sharedPref.registerOnSharedPreferenceChangeListener(this);
        if( useLocation ) mll.checkProvider();
    }
	
	@Override
    public boolean onCreateOptionsMenu( Menu menu )
	{
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.appmenu, menu);
        
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        // Handle item selection
        switch( item.getItemId() )
        {
        	case R.id.menuSettings:
        		this.startActivity( new Intent(this, SettingsActivity.class) );
	            return true;
	        case R.id.menuExit:
	        	releaseCamera();
	            sharedPref.unregisterOnSharedPreferenceChangeListener(this);
	    		this.finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
        }
    }
    
    public void setupCamera()
    {
    	// Create an instance of Camera
        mCamera = getCameraInstance();
        
    	// get Camera parameters
        Camera.Parameters params = mCamera.getParameters();
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
        // set Camera parameters
        mCamera.setParameters(params);
        
        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview( this, mCamera );
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
    }
	
	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance()
	{
		Camera c = null;
	    try
	    {
	        c = Camera.open(); // attempt to get a Camera instance
	    }
	    catch( Exception e )
	    {
	        // Camera is not available (in use or does not exist)
	    }
	    return c; // returns null if camera is unavailable
	}
	
	private void releaseCamera()
	{
        if( mCamera != null )
        {
            mCamera.release();        // release the camera for other applications
            mCamera = null;
            
            mPreview.getHolder().removeCallback(mPreview);
        }
    }
	
	private PictureCallback mPicture = new PictureCallback()
	{
	    @Override
	    public void onPictureTaken( byte[] data, Camera camera )
	    {
	    	nth.uploadImageFile(data);
			camera.startPreview();
	    }
	};

	public void onSharedPreferenceChanged( SharedPreferences sharedPreferences, String key )
	{
		if (key == null) return;
		Log.d("debuf", key);
		if( key.equals(GeoLAPP.PREFERENCE_KEY_PREF_SERVER) )
		{
			nth.setUpLoadServerImageUri( sharedPref.getString(key, "") );
		}
		if( key.equals(GeoLAPP.PREFERENCE_KEY_PREF_LOCATION) )
		{
			useLocation = sharedPref.getBoolean( key, false );
			if( useLocation == true )
			{
				LocationManager lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);
				mll = new MyLocationListener( lm, this );
			}
			else
			{
				mll.disableLocation();
			}
		}
	}
	
}
