package imageLocator.network;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

public class NetworkHelper
{
	private String upLoadServerImageUri;
	private int serverResponseCode;
	
	private Activity parent;
	
	public NetworkHelper( Activity parent, String server )
	{
		this.upLoadServerImageUri = server;
		this.serverResponseCode = 0;
		this.parent = parent;
	}
	
	public void uploadImageFile( byte[] byte_arr )
	{
		final byte[] fdata = byte_arr;
    	
    	new Thread()
    	{
    		@Override
    		public void run()
    		{
    			doUpload(fdata);
			}
		}.start();
	}
	
	private void doUpload( byte[] byte_arr )
	{
		String timeStamp = new SimpleDateFormat( "yyyyMMdd_HHmmss", Locale.getDefault() ).format(new Date());
		String fileName = "IMG_" + timeStamp + ".jpg";

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		ByteArrayInputStream byteInputStream = new ByteArrayInputStream( byte_arr );

		try {
			URL url = new URL(upLoadServerImageUri);
			conn = (HttpURLConnection) url.openConnection(); // Open a HTTP  connection to  the URL
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			
			dos = new DataOutputStream(conn.getOutputStream());

			dos.writeBytes( twoHyphens + boundary + lineEnd );
			dos.writeBytes( "Content-Disposition: form-data; name=\"files\";filename=\""+ fileName + "\"" + lineEnd );
			dos.writeBytes( lineEnd );

			bytesAvailable = byteInputStream.available(); // create a buffer of  maximum size

			bufferSize = Math.min( bytesAvailable, maxBufferSize );
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = byteInputStream.read( buffer, 0, bufferSize );

			while( bytesRead > 0 )
			{
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = byteInputStream.available();
				bufferSize = Math.min( bytesAvailable, maxBufferSize );
				bytesRead = byteInputStream.read( buffer, 0, bufferSize );
			}

			// send multipart form data necessary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			serverResponseCode = conn.getResponseCode();
			final String serverResponseMessage = conn.getResponseMessage();
			
			InputStream loc_is = conn.getInputStream();
			BufferedReader r = new BufferedReader(new InputStreamReader(loc_is));
			StringBuilder total = new StringBuilder();
			String line;
			while( (line = r.readLine()) != null )
			{
			    total.append(line);
			}
			final String location = total.toString();

			Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);
			if( serverResponseCode == 200 )
			{
				parent.runOnUiThread(new Runnable()
				{
					public void run()
					{
						Log.i( "Location computed", location );
						Toast.makeText( parent, "Location computed: " + location,
								Toast.LENGTH_LONG).show();
					}
				});
			}

			//close the streams //
			byteInputStream.close();
			dos.flush();
			dos.close();

		}
		catch( MalformedURLException ex )
		{
			ex.printStackTrace();
			parent.runOnUiThread( new Runnable()
			{
				public void run()
				{
					Toast.makeText(parent, "MalformedURLException", Toast.LENGTH_SHORT).show();
				}
			});
			Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
		}
		catch (Exception e)
		{
			final Exception te = e;
			e.printStackTrace();
			parent.runOnUiThread(new Runnable()
			{
				public void run()
				{
					Toast.makeText(parent, "Exception : " + te.getMessage(), Toast.LENGTH_SHORT).show();
				}
			});
			Log.e("Upload file to server Exception", "Exception : " + e.getMessage(), e);
		}
	}
	
	public void setUpLoadServerImageUri( String serverURI )
	{
		this.upLoadServerImageUri = serverURI;
	}
	
}
