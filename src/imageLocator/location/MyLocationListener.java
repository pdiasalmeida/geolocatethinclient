package imageLocator.location;

import imageLocator.main.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MyLocationListener implements LocationListener
{
	private LocationManager locationManager;
	private Activity parent;
	
	public boolean locationEnabled;
	
	public String locationProvider;
	public Location currentLocation;
	
	private static final int TIME_INTERVAL = 1000 * 60 * 1;
	//private static final long MIN_TIME_BTW = (long) (1000 * 60 * 1);

	public MyLocationListener( LocationManager lm, Activity parent )
	{
		this.locationManager = lm;
		this.parent = parent;
		
		this.currentLocation = null;
		this.locationEnabled = true;
	}
	
	public void checkProvider()
	{
		if( networkProviderIsEnabled() )
		{
			locationProvider = LocationManager.NETWORK_PROVIDER;
			locationManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER, 0, 0, this );
			currentLocation = locationManager.getLastKnownLocation( locationProvider );
		}
		else
		{
			menuProvider();
		}
	}
	
	public boolean networkProviderIsEnabled()
	{
		return locationManager.isProviderEnabled( LocationManager.NETWORK_PROVIDER );
	}
	    
	public void menuProvider()
	{
		final Dialog dialog = new Dialog( parent );
		dialog.setContentView( R.layout.location_provider_menu );
		dialog.setTitle( "Serviços de localização." );
		dialog.setCancelable( false );

		final Button locationSettings = (Button) dialog.findViewById( R.id.lp_sett_button );
		locationSettings.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
				parent.startActivityForResult( new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0 );
			}
		});

		Button locationCancel = (Button) dialog.findViewById( R.id.lp_cancel_button );
		locationCancel.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dialog.dismiss();
				locationEnabled = false;
				Toast.makeText( parent, "Não vão ser utilizados serviços de localização.", Toast.LENGTH_LONG).show();
			}
		});
		
		dialog.show();
	}
	
	public void disableLocation()
	{
		locationManager.removeUpdates( this );
		locationEnabled = false;
		currentLocation = null;
	}
	
	@Override
	public void onLocationChanged( Location location )
	{
		Log.d("location", "lat: " + location.getLatitude() + " lon: " + location.getLongitude() );
		if( isBetterLocation(location, currentLocation) )
		{
			currentLocation = location;
		}
	}

	@Override
	public void onStatusChanged( String provider, int status, Bundle extras ) {}

	@Override
	public void onProviderEnabled( String provider ) {}

	@Override
	public void onProviderDisabled(String provider) {}

	public boolean hasLocation()
	{
		return currentLocation != null;
	}
	
	protected boolean isBetterLocation( Location location, Location currentBestLocation )
	{
		if( currentBestLocation == null )
		{
	        // A new location is always better than no location
	        return true;
	    }

	    // Check whether the new location fix is newer or older
	    long timeDelta = location.getTime() - currentBestLocation.getTime();
	    boolean isSignificantlyNewer = timeDelta > TIME_INTERVAL;
	    boolean isSignificantlyOlder = timeDelta < - TIME_INTERVAL;
	    boolean isNewer = timeDelta > 0;

	    // If it's been more than two minutes since the current location, use the new location
	    // because the user has likely moved
	    if( isSignificantlyNewer )
	    {
	        return true;
	    // If the new location is more than two minutes older, it must be worse
	    }
	    else if( isSignificantlyOlder )
	    {
	        return false;
	    }

	    // Check whether the new location fix is more or less accurate
	    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	    boolean isLessAccurate = accuracyDelta > 0;
	    boolean isMoreAccurate = accuracyDelta < 0;
	    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

	    // Check if the old and new location are from the same provider
	    boolean isFromSameProvider = isSameProvider(location.getProvider(),
	            currentBestLocation.getProvider());

	    // Determine location quality using a combination of timeliness and accuracy
	    if( isMoreAccurate )
	    {
	        return true;
	    }
	    else if( isNewer && !isLessAccurate )
	    {
	        return true;
	    }
	    else if( isNewer && !isSignificantlyLessAccurate && isFromSameProvider )
	    {
	        return true;
	    }
	    return false;
	}

	/** Checks whether two providers are the same */
	private boolean isSameProvider( String provider1, String provider2 )
	{
	    if( provider1 == null )
	    {
	      return provider2 == null;
	    }
	    return provider1.equals(provider2);
	}

}
